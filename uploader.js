var inp = document.querySelector(".chooser");
var block = document.querySelector(".window");
var arr = [];
var url = "http:192.168.0.107";

function clearfield() {
  document.querySelector(".chooser").value = null;
}

var container = document.querySelector(".container");
var btn = document.createElement("button");
btn.classList.add("upload");
btn.innerHTML = "Upload";

container.addEventListener("dragover", (e) => {
  e.preventDefault();
  container.classList.add("container--dragOver");
});

container.addEventListener("dragleave", (e) => {
  container.classList.remove("container--dragOver");
});

container.addEventListener("drop", (e) => {
  container.classList.remove("container--dragOver");
  e.preventDefault();
  container.append(btn);
  arr = e.dataTransfer.files;
  block.innerHTML = "";
  draw();

  btn.onclick = function () {
    send();
  };
});

inp.addEventListener("change", function (e) {
  arr = inp.files;
  container.append(btn);
  block.innerHTML = "";
  draw();

  btn.onclick = function () {
    send();
  };
});

function draw() {
  for (let i = 0; i < arr.length; ++i) {
    let item = document.createElement("li");
    let iconBox = document.createElement("div");
    let icon = document.createElement("img");
    let about = document.createElement("div");
    let filesName = document.createElement("span");
    let progressBar = document.createElement("div");
    let bar = document.createElement("progress");
    let percent = document.createElement("span");

    iconBox.classList.add("icon-box");
    icon.classList.add("icon");
    icon.setAttribute("src", "image/download.png");
    iconBox.append(icon);
    about.classList.add("about");
    filesName.classList.add("file-name");
    filesName.append(arr[i].name);
    about.append(filesName);
    progressBar.classList.add("progress-bar");
    bar.setAttribute("max", "100");
    progressBar.append(bar);
    about.append(progressBar)
    percent.classList.add("percent");
    progressBar.append(percent);
    item.classList.add("window__item");
    item.setAttribute("data-test", arr[i].name);
    item.append(iconBox);
    item.append(about);
    block.append(item);
  }
}

let step = 3;
let x = 0;

function send() {
  for (let i = x; i < x + step; ++i) {

    if (arr[i]) {
      let xhr = new XMLHttpRequest();
      let formdata = new FormData();
      formdata.append("file", arr[i]);
      let filesLength = arr.length;
      let fileName = arr[i].name;
      xhr.onreadystatechange = function (e) {
        if (this.readyState == XMLHttpRequest.DONE) {
          let data = JSON.parse(this.responseText);
          if (data.status) {
            x++;
            if (x % step == 0) {
              send();
            }
          }
        }

        if (filesLength == i + 1 && xhr.readyState == 4) {
          setTimeout(() => {
            alert("Uploaded Successfully");
            clearfield();
            x=0;
          }, 400);
        }
      };

      xhr.upload.onprogress = function (e) {
        let loaded = parseInt((e.loaded / e.total) * 100);
        let item = document.querySelector(
          `.window__item[data-test='${fileName}']`
        );
        item.querySelector(".percent").innerText = `${loaded}%`;
        item.querySelector("progress").setAttribute("value", loaded);
      };

      xhr.open("post", url);
      xhr.send(formdata);
    }
  }
}
